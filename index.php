<?php

	//no-cache
	header('Cache-Control: no-cache, must-revalidate, public');
	header('Pragma: no-cache');
	//binary file
	//header('Content-Transfer-Encoding: binary');
	//header('Content-Security-Policy "default-src 'self'"');
	//header('Public-Key-Pins "pin-sha256=\"base64+primary==\"; pin-sha256=\"base64+backup==\"; max-age=5184000; includeSubDomains"');
	//header('X-Content-Type-Options: nosniff');
	header('X-Frame-Options: DENY');
	header('X-XSS-Protection: 1; mode=block');

?>
<!DOCTYPE html>
<html lang="en">

<?php include_once("header.php"); ?>
<body>
	<!-- <script src="js/analytics.js"></script> -->

	<main>

		<section class="section-principal">
			<!-- Copy de dinàmica-->
			<article class="copy-principal">
					<p class="text-center"><span class="text-black">ENVÍA</span> <br> <span class="text-light">A TUS SERES QUERIDOS</span><br>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-right d-none d-sm-inline" aria-hidden="true"></i>
					<span class="text-mlight text-mlight-mobile">UN GESTO DE CARI&Ntilde;O</span><!--  <span class="text-black-regalo">#ELREGALOPERFECTO</span> -->
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<i class="fa fa-caret-left d-none d-sm-inline" aria-hidden="true"></i>
					<br>
					<span class="text-light-viñeta">EN 2 SENCILLOS PASOS:</span></p>

					<figure class="text-center img-fluid arrow-down">
						<img src="img/arrow-down.png" alt="Flecha Abajo">
					</figure>

			</article>

		</section>

		<section class="container-fluid mb-4">
			<article class="row">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
				  <div class="carousel-inner" role="listbox">
				    <div class="carousel-item active">
							<!-- <img class="d-block img-fluid" src="img/Steps.png" alt="First slide" style="margin:0 auto;"> -->
							<div class="parrafo">
								<span> <strong>1</strong> <br>paso</span>
								<p>Escribe en el cuadro de texto el nombre de esa persona a quien le vas a dedicar este chocolate</p>
							</div>
							<div class="parrafo">
								<span> <strong>2</strong> <br>paso</span>
								<p>Pulsa el botón para compartir en Facebook o descarga una historia para Instagram</p>
							</div>
				    </div>
				  
				  </div>

			

				</div>
			</article>
		</section>


		<!-- canvas del chocolate -->
		<article class="container-canvas">
			
			<div class="loader">
				<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
				<span class="sr-only">Loading...</span>
			</div>
			
			<canvas class="imgGenContainer container" id="chocoName" style="position: relative;">Your browser does not support the HTML5 canvas tag.</canvas>
			<canvas class="chocoCanvas" id="miCanvas" style="display: none; ">Your browser does not support the HTML5 canvas tag.</canvas>

			<figure class="imgGenContainer container" style="position: relative; display: none;">

				<img src="img/Chocolate.png" class="choco1 img-fluid" alt="Chocolate centro" id="pintadoTexto">

				<img src="" alt="" id="imgGenerada" class="img-fluid mt-5">

				<div class="pintadoTexto" style=" font-family: Amarillo !important;top: 34.3%; transform: translateY(-50%); position: absolute; left: 0; right: 0; margin: 0 auto; /* width: auto; */ text-align: center; text-transform: capitalize; text-align: center; color: white !important;">

				</div>
				<!-- <input type="text" id="pintadoTexto" value=""> -->
			</figure>
		</article>

		<!-- Imagen generada por canvas -->

		<form class="container">
			<div class="row">

				<input type="text" name="inputChoco" class="form-control col-10 offset-1 offset-sm-1 col-sm-9 col-md-7 form-control-md inputChoco" id="inputChoco" maxlength="15" placeholder="Máximo 15 caracteres.">

				<button type="submit" class="btn btn-gris btn-md col-10 offset-1 offset-sm-0 col-sm-1 col-md-3 vistaPrevia" id="vistaPrevia" formmethod="POST"><i class="fa fa-eye " aria-hidden="true"></i> <span class="d-inline d-sm-none d-md-inline">Vista previa</span></button>

			</div>
		</form>

		<div class="share-container container" style="margin-top: 10%; ">

			<div class="row downbut">
				<div class="downloader">
					<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
					<span class="sr-only">Loading...</span>
				</div>
				<a rel="norefer" class="btn btn-info col-10 mt-1 offset-sm-4 col-sm-6 col-md-12 offset-md-0" id="guardaCanvas" href="" download><i class="fa fa fa-download pr-2" aria-hidden="true"></i>Descargar</a>
				<button class="btn btn-info col-10 mt-1 offset-sm-4 col-sm-6 col-md-12 offset-md-0" id="guardaCanvasIE" style="display: none;" target="_blank"><i class="fa fa fa-download pr-2" aria-hidden="true"></i>Descargar</button>
			</div>

			<div class="row">
				<button type="button" id="shareBtn" class="btn btn-primary mt-1 input-sm col-10 offset-sm-4 col-sm-6 col-md-12 offset-md-0"><i class="fa fa-facebook-official pr-2" aria-hidden="true"></i>Compartir</button>
			</div>
		</div>

		<div class="mt-5 contenedor-copyfinal">
				<p class="text-mlight text-center">Una vez que compartas <span class="text-black">#ElRegaloPerfecto </span> <br>
				sigue nuestras redes para que te enteres de esta sorpresa</p>
				<span class="rrss-container text-black text-center">
					<a href="https://www.facebook.com/savoynestle/" target="_blank"><i class="fa fa-facebook pr-2" aria-hidden="true"></i><span class="rrss-text" style="display: none;">Savoy Nestle</span></a>

					<a href="https://twitter.com/NestleSavoy" target="_blank"><i class="fa fa-twitter pr-1" aria-hidden="true"></i><span style="display: none;" class="rrss-text">NestleSavoy</span></a>

					<a href="https://www.instagram.com/savoynestle/?hl=es" target="_blank"><i class="fa fa-instagram pl-1 pr-1" aria-hidden="true"></i><span class="rrss-text" style="display: none;">@savoynestle</span></a>
				</span>
		</div>
	</main>
	<div class="c-holder" style="display: none;">
		<i class="fa fa-close"></i>
		<img class="chocoHolder" src="" style="width: 100%; margin: 0 auto;">
	</div>
	<!-- Filtro de palabras -->
	<script src="js/filtro.js"></script>

	<!-- Canvas -->
	<script src="js/canvas.js"></script>
	<!-- Fin Canvas -->
	<figure style="position: relative;" class="d-none d-md-block">
		<img src="img/pestana-nestle.png" alt="Nestle Savoy" style="position: fixed; bottom: 0; right: 0; bottom: 0px; max-width: 25%;">
	</figure>
	<div id="result"></div>
	
	<script type="text/javascript">

		$('.fa-close').click(function(){
			$('main').fadeIn();
			$('.chocoHolder').removeAttr('src');
			$('.c-holder').fadeOut();
		});
		// share 
		document.getElementById('shareBtn').onclick = function() {
				var cover = $('meta[name=cover]').attr('content');
				console.log(cover);
				var BASE_URL = window.location.href ;
			 	FB.ui({
					method: 'share_open_graph',
					action_type: 'og.shares',
					app_id: '323224598146779',
					hashtag: '#ElRegaloPerfecto',
					redirect_uri: 'http://savoy.com.ve',
					action_properties: JSON.stringify({
						object : {
							'og:url': BASE_URL, 
							'og:title': 'Cremosa Navidad',
							'og:site_name': 'SAVOY',
							'og:url': 'http://savoy.com.ve/cremosa-navidad',
							'og:description': 'Haz #ElRegaloPerfecto, ingresa aquí y lo descubrirás!',
							'og:image': cover,
							'og:image:width': '1200',
							'og:image:height': '630',
						}
					})
				});
				
		};

		//para el mongolo IE
		var version = detectIE();
			if (version === false) {
				// document.getElementById('result').innerHTML = '<s>IE/Edge</s>';
			} else if (version >= 12) {
				// alert('esto es edge y es medio decente');
				IEbutton();
				// document.getElementById('result').innerHTML = 'Edge ' + version;
			} else {
				// alert('esto es IE y apesta!');
				IEbutton();
				// document.getElementById('result').innerHTML = 'IE ' + version;
			}

			// add details to debug result
			// document.getElementById('details').innerHTML = window.navigator.userAgent;
			
			function detectIE() {
				var ua = window.navigator.userAgent;

				// Test values; Uncomment to check result …

				// IE 10
				// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
				
				// IE 11
				// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
				
				// Edge 12 (Spartan)
				// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
				
				// Edge 13
				// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

				var msie = ua.indexOf('MSIE ');
				if (msie > 0) {
					// IE 10 or older => return version number
					return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				}

				var trident = ua.indexOf('Trident/');
				if (trident > 0) {
					// IE 11 => return version number
					var rv = ua.indexOf('rv:');
					return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				}

				var edge = ua.indexOf('Edge/');
				if (edge > 0) {
					// Edge (IE 12+) => return version number
					return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				}

				// other browser
				return false;
			}
			
			function IEbutton(){
				document.getElementById('guardaCanvas').setAttribute("style", "display: none;");
				document.getElementById('guardaCanvasIE').setAttribute("style", "display: block;");
			}
			function mobileButton(){
				document.getElementById('guardaCanvas').setAttribute("style", "display: none;");
				document.getElementById('guardaCanvasIE').setAttribute("style", "display: none;");
				document.getElementById('guardaCanvasMov').setAttribute("style", "display: block;");
			}
	
	</script>
	<div class="bg"></div>
	
</body>
</html>
