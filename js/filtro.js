
var myCustomCode = (function($){
	$(document).ready(function(){

		var checkHasWord = function(arr,word){
			var check = false;
			var popLastCharFromArrItem;
			for(var i = 0; i < arr.length; i++){
				popLastCharFromArrItem = arr[i].substr(0,arr[i].length-1);
				if(word.indexOf(arr[i]) > -1 || word.indexOf(popLastCharFromArrItem) > -1){
					check = true;
					break;
				}else{
					check = false;
				}
			}
			return check;
		};

		var removeBadWord = function(data,toremove){
			var strdata,strarr,str;
			strdata = data;
			strdata = strdata.charAt(0).toUpperCase() + strdata.slice(1).toLowerCase();;
			strdata = strdata.replace(/[^a-zA-Z ]/g, "");
			strarr = strdata.split(" ");
			str = strarr.reduce(function(all,item,index){
				var sorted,len,baseChar,modstr,hasword;
				sorted = toremove;
				len = item.length;
				baseChar = "";
				hasword = checkHasWord(sorted,item);

				if(hasword){
					modstr = baseChar.repeat(len);
					all[index] = modstr;
				}else{
					all[index] = item;
				}
				return all;
			},[]).join(" ");
			return str;
		};

		$(".vistaPrevia").on("click",function(e){
			e.preventDefault();
			var deleteword = ["Anormal","Acaba","Mierda","Coño","Jodete","Marico","Guebo","Guevo","Totona","Maldito","Maldita","Puta","Puto","Perra","Perro","Bastardo","Bastarda","Malparido","Malparida","Ano","Arrecho","Arrecha","Bachaquero","Bachaquera","Bolivariano","Bolivariana","Bruja","Brujo","Bisexual","Birra","Birriondo","Babalao","Boleta","Carajo","Carajita","Carajito","Cabron","Cabezadewebo","Chavez","Coñoemadre","Coñodetumadre","Coñodelamadre","Cuca","Chavista","Caliche","Contrabando","Carajo","Caraja","Culo","CULO","Clitoris","Culopeludo","Coñazo","Crimen","Capriles","Concha","Caca","Cagon","Cagada","Desgraciado","DESGRACIADO","Droga","Diosdado","Diablo","DIOSDADO","Durex","Dildo","Estupido","Engendro","Escoñetar","Escoñetao","Enchufado","Enano","Enema","Excitado","Fea","Feo","Francotirador","Fusil","Frotar","Fastidioso","Frotar","Follar","Fuete","Flujo","Gnb","GNB","Gargajo","Guardianacional","Guardianacionalbolivariana","Gemir","Gay","GAY","Guevon","Guevón","Guebón","Guebon","Hediondo","Hijodeputa","Hijoeputa","GNB","GUARDIANACIONAL","FROTAR","FOLLAR","FUETE","FLUJO","GARGAJO","GUARDIANACIONALBOLIVARIANA","GEMIR","GAY","GUEBÓN","GUEBÓN","GUEBON","GUEVON","HEDIONDO","HIJODEPUTA","HIJOEPUTA","HORROROSO","HORROROSA","Horroroso","Horrorosa","Hijodeperra","Hijoeperra","HIMEN","IMBECIL","IDIOTA","INUTIL","INMAMABLE","JODER","JODA","JUEPUTA","JALABOLA","JULEPE","KUKA","KOÑO","KULO","Himen","Imbecil","Idiota","Inmamable","Inutil","Joder","Joda","Jueputa","Jalabola","Julepe","Kuka","Koño","Kulo","KKK","LAMEBOLA","LAMECUCA","LAMECULO","LAMETETA","LAMEESCROTO","Lameculo","Lamebola","Lamecuca","LADILLA","LECHE","LACAVA","LAGAÑA","LOBA","LOLA","LAMER","LAMER","LEPE","LEPRA","MIERDA","MAMAGUEVO","MADURO","MARICO","MARICA","Ladilla","Leche","Lacava","Lagaña","loba","Lola","Lepra","Lepe","Maduro","Mamaguevo","maduro","MALDITOMADURO","Malditomaduro","MaduroMaldito","Mardita","MARDITA","MARDITO","MALPARIDO","MALNACIDO","MALNACIDA","MALPARIDA","MUJERZUELA","MARIPOSON","MARICON","MARIQUITO","MATAR","MARIHUANA","NOJODA","NIE","ORTO","OPOSITOR","OPOSICIÓN","Mujerzuela","Mariposon","Maricon","Mariquito","Matar","Marcha","Maton","Marihuana","Nie","Orto","Oposicion","Opositor","PAJUO","Pajuo","PAPO","Papo","PEO","Peo","PELABOLA","Pelabola","Pato","PATO","PUTA","PATRIA","Patria","PERRA","PENE","Pene","PIPI","Pipi","PANOCHA","Panocha","QK","Qk","QLO","Qlo","QUESO","Queso","QUESUO","Quesuo","ROLITRANCO","Rolitranco","RAJA","Raja","RAJAR","Rajar","RELAMBE","Relambe","Relambepipi","Relambecuca","Relambeculo","Relambepene","Relambeguevo","RELAMBECULO","RELAMBEPIPI","RELAMBECUCA","RELAMBEGUEVO","REVOLUCIÓN","Revolución","REVOLUCION","Revolucion","RESISTENCIA","Resistencia","RIDICULO","Ridiculo","RATA","Rata","SEMEN","SAPO","SAPA","Sapo","Sapa","SEMERENDO","Semerendo","SEXO","sexo","Sexoanal","SEXOANAL","SADICO","Sadico","SUCIO","SUCIA","Sucio","Sucia","SADOMASOQUISMO","Sadomasoquismo","SORRA","Sorra","SUICIDIO","Suicidio","Sobaco","SOBACO","MUD","Mud","SIDA","Sida","SIFILIS","Sifilis","Teta","TETA","TOTONA","TACAZO","Tacazo","TETICA","VIOLADOR","Violador","VIOLENCIA","Violencia","VOMITO","Vomito","VIRGEN","VIRGO","Virgen","Virgo","VEJIGA","VERGA","Verga","Webo","Huevo","HUEVO","Cabeza","CABEZA","ZORRA","Zorra"];
			var palabras = $(".inputChoco").val();
			var modifiedData = removeBadWord(palabras, deleteword);
			$(".pintadoTexto").show(function(){
				$(this).text(modifiedData).show();
				$(".inputChoco").val(modifiedData);
			});

		});

	});
})(jQuery);
