
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1112881435513090',
      xfbml      : true,
      version    : 'v2.10'
    });

    FB.AppEvents.logPageView();
    function onButtonClickDownload() {
	  // Add this to a button's onclick handler
	  FB.AppEvents.logEvent("Descargó la foto");
  	}
   function onButtonClickShare() {
	  // Add this to a button's onclick handler
	  FB.AppEvents.logEvent("Compartió en su muro");
	}

  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
  
