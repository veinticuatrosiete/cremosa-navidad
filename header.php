<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="img/favicon.png" />
	<title> SAVOY - CREMOSA NAVIDAD - LANDING COMPARTE #ELREGALOPERFECTO</title>
	<!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#600301">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#600301">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#600301">
	<meta property="fb:app_id" content="323224598146779" />
	<meta property="og:type" content="website" />
	<meta name="cover" property="og:image" content="http://lab.hacemosloquenosgusta.com/savoy/cremosa-navidad/img/cover.jpg" />
	<meta property="og:title" content="SAVOY - Cremosa Navidad" />
	<meta property="og:url" content="https://savoy.com.ve/cremosa-navidad/"/>
	<meta property="og:description" content="Porque estemos donde estemos, quiero que juntos compartamos esta cremosa navidad #ElRegaloPerfecto" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script> -->
	<!-- Fuentes de Google -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,900" rel="stylesheet">
	<script src="https://use.fontawesome.com/43e122f8a2.js"></script>

	<!-- Fin fuentes -->

	<!-- Estilos -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Latest compiled and minified CSS -->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://connect.facebook.net/en_US/all.js" async=""></script>

	<!-- Anti-clickjacking -->
	<script src="js/anticlickjacking.js"></script>

	<!-- xframe-scripting -->
	<script src="js/xframe.js"></script>

	<!-- Fin estilos -->

	<style id="antiClickjack">body{display:none !important;}</style>
	<script type="text/javascript">
		if (self === top) {
			var antiClickjack = document.getElementById("antiClickjack");
			antiClickjack.parentNode.removeChild(antiClickjack);
		} else {
			top.location = self.location;
		}
		// FB SDK
		window.fbAsyncInit = function() {
			FB.init({
					appId   : '323224598146779',
					oauth   : true,
					status  : true, // check login status
					cookie  : true, // enable cookies to allow the server to access the session
					xfbml   : true, // parse XFBML
					version	: 'v2.10'
			});

		};
	</script>

</head>
